# trilium-hexmap

Version 1.5

Hexmap html+js widget for [Trilium Notes](https://github.com/zadam/trilium).

## What is this?

Fetches notes with specific labels to be displayed on the hexmap, showing the note data in various ways (coordinates, labels, icons and hex color), uses the widget's promoted or pre-set attributes to easily set the hexmap values (max x/y size, range to be displayed, scale...), and can optionally a background image by linking to an image note in trilium or by URL.

Used for my own personal purposes (prepping and tracking a Pathfinder 2e tabletop campaign) and might require tweaking to be used for other label attributes (if you already use templates and attribute automation a lot) and note structures.

![Preview](https://user-images.githubusercontent.com/12102823/138761049-802dc78f-c628-4841-bfdf-f133989ae81d.png)

## Install

Download this repo and extract it.
Inside of Trilium, right click the folder you want to import into and click "Import into Note" at the bottom.
Select the "Hexmap Widget Import.zip" archive.
Import without changing any of the the default options (although if you get issues maybe try unchecking stuff like Safe Import which i'm assuming can sometimes decide to not import things as they are, although I've never had problems).
The Hexmap Widget is then imported but nothing is displayed yet, as indicated by the message "Render HTML doesn't have required relation to function properly", because the renderNote attribute has been automatically disabled on import. To fix this, go to the Owned attributes tab at the top and look for "~disabled:renderNote=render.html", normally the last attribute in the list. Click on it and remove the "disabled:" part so as to have "renderNote" as the attribute name. Save and now the widget should display.

## Usage

BE AWARE: If you want to use a background image that already has a grid, it can often be fiddly to get it to line up. The image can't have any margins around the grid, and the proportional width/height of hexes might not be the same as your image. If your image with an existing grid can't quite match up you might fix it by squeezing/stretching the width or height of your image accordingly.

I've included an example background image (with no grid) with working attributes already set up (editable in the Owned attributes tab).

### Hex grid layout

By default, this widget is in vertical layout (the columns are aligned and not the rows) with "odd" shifting (every odd column/line shoved down, starting at 0). You can toggle horizontal layout (the rows are aligned and not the columns) by changing the "grid_horizontal_toggle" label attribute to true, and you can toggle the "even" shifting (every even column/line shoved down, starting at 0) by changing the "grid_even_toggle" label attribute to true.
For examples of what these different layouts look like if you're not sure, here's visual examples explaining it : https://www.redblobgames.com/grids/hexagons/#coordinates-offset

Coordinates for rows and columns start at 1 and not 0 since that's the default for all hexmaps I've come across, and I think that makes more sense for most people.

### Attributes

Here's a quick explanation of the rest of the attributes:

- full_width and full_height represent the TOTAL number of columns (x or width) and lines (y or height) of the map (in my case 23 columns and 15 lines total).
- range_x and range_y can optionally restrict the area that will be rendered inside of the total size. You specify the range of columns and lines (in format "start,end") that you want to be rendered. 
This is useful if you have a huge full map and you only need a smaller area rendered most of the time, since a big map can be slow to render or way annoying to navigate/scroll. 
Also, since the widget automatically centers the view when rendered, if you specify a range it will center the view within that range, which means you can focus on a particular corner of the map you're working on and it will only render that and properly center the view whenever you render the widget rather than have to scroll a bunch every time.
- scale is the zoom level (technically the height in pixels of each hex which is used as a kind of scale when rendered). 
The smaller the value, the more "zoomed out" the view since each hex is smaller, the higher the value the more "zoomed in" since each hex is rendered bigger.
There are +/- buttons at the bottom right of the widget which modify this value, but the content has to be re-drawn every time which can look a big strange, I have found no other simple way to do this so far.
- bg_image_note is a relation attribute which fetches an internal image type note to be used as the background image.
- bg_image_url is a label attribute for an external URL if your background image is hosted somewhere else.
- new_note_root is a relation attribute which specifies where (under which note/folder) you want automatically created notes to be put, when you double-click the label of an empty hex.
- new_note_template is an optional relation attribute which specifies a note template to use when creating new notes by double-clicking.
    
### Automatic Note Integration
    
There's a bunch of examples notes included which explain how to have your own "location" and "agent" notes be fetched by the hexmap widget to be displayed on the hexmap itself. The location notes use the "hex_coords" label attribute to be recognized by the widget (corresponding to the X and Y coordinates of the hex separated by a dot, in the format "X.Y"), and the agent notes use a "current_location" relation attribute which links to a location note. Of course this is tailored to my own needs and templates, but with the included working example you can easily adapt and use your own templates and attributes (changing the names of the attributes fetched by the widget in the "script.js" file).

Feel free to modify the code to adapt your own needs!
