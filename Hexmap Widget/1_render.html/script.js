/**
 * Version 1.7
 **/
"use strict";

const contentContainer = document.getElementById("hexmap-container");
const viewContainer =
  $(contentContainer)[0].parentNode.parentNode.parentNode.parentNode.parentNode;

initializeUI(viewContainer);
void renderHexmap(contentContainer, viewContainer);
void makeDragScrollable(viewContainer);

function initializeUI() {
  // Bind +/- zoom buttons
  document.getElementById("zoom_in").onclick = function () {
    void clickZoom(1);
  };
  document.getElementById("zoom_out").onclick = function () {
    void clickZoom(0);
  };
}

// Function to zoom in/out the render scale
async function clickZoom(zoomDirection) {
  await api.runOnBackend(
    (zoomDirection) => {
      const parentNote = api.startNote.getParentNotes()[0];
      const scale = parseInt(parentNote.getLabelValue("scale"), 10);
      const newScale = scale + (zoomDirection === 1 ? 10 : -10);
      parentNote.setLabel("scale", parseInt(newScale, 10).toString());
    },
    [zoomDirection],
  );
  await renderHexmap(contentContainer, viewContainer);
}

async function renderHexmap(contentContainer, viewContainer) {
  // Grab widget attributes
  const map_parameters = await api.runOnBackend(() => {
    const parentNote = api.startNote.getParentNotes()[0];
    // hex grid generation values
    const xStart = parseInt(
      parentNote.getLabelValue("range_x").toString().split(",")[0],
      10,
    );
    const xEnd = parseInt(
      parentNote.getLabelValue("range_x").toString().split(",")[1],
      10,
    );
    const xMax = parseInt(parentNote.getLabelValue("full_width"), 10);
    const yStart = parseInt(
      parentNote.getLabelValue("range_y").toString().split(",")[0],
      10,
    );
    const yEnd = parseInt(
      parentNote.getLabelValue("range_y").toString().split(",")[1],
      10,
    );
    const yMax = parseInt(parentNote.getLabelValue("full_height"), 10);
    // Grid modes
    const gridEvenToggle = parentNote.getLabelValue("grid_even_toggle");
    const gridHorizontalToggle = parentNote.getLabelValue(
      "grid_horizontal_toggle",
    );
    // Map rendering
    const bgImageUrl = parentNote.getLabelValue("bg_image_url");
    const bgImageNote = parentNote.getRelationTarget("bg_image_note");
    const hexHeight = parseInt(parentNote.getLabelValue("scale"), 10);
    // note creation relations
    const newNoteRoot = parentNote.getRelationTarget("new_note_root");
    const newNoteTemplate = parentNote.getRelationTarget("new_note_template");
    return {
      parentNote: parentNote.getPojo(),
      xStart,
      xEnd,
      xMax,
      yStart,
      yEnd,
      yMax,
      gridEvenToggle,
      gridHorizontalToggle,
      bgImageUrl,
      bgImageNote: bgImageNote.getPojo(),
      hexHeight,
      newNoteRoot: newNoteRoot.getPojo(),
      newNoteTemplate: newNoteTemplate.getPojo(),
    };
  }, []);

  // /** Prepare Map Grid **/
  contentContainer.innerHTML = "";
  // Set Start values, Start value can't be lower than 1
  let xStart = map_parameters.xStart < 1 ? 1 : map_parameters.xStart;
  let yStart = map_parameters.yStart < 1 ? 1 : map_parameters.yStart;
  // Set End values, End value can't be higher than Max value, can't be lower than 2
  let xEnd =
    map_parameters.xEnd > map_parameters.xMax
      ? map_parameters.xMax
      : map_parameters.xEnd < 2
      ? 2
      : map_parameters.xEnd;
  let yEnd =
    map_parameters.yEnd > map_parameters.yMax
      ? map_parameters.yMax
      : map_parameters.yEnd < 2
      ? 2
      : map_parameters.yEnd;

  // Set hex dimensions from height
  let hexHeight = map_parameters.hexHeight ? map_parameters.hexHeight : 100;
  let hexHeightOriginOffset = hexHeight / 2;
  let hexSide = hexHeight / Math.sqrt(3);
  let hexWidth = hexSide * 2;
  let hexWidthOriginOffset = (hexWidth - hexSide) / 2;

  // Toggles for even/odd shifting and for vertical/horizontal alignment
  let gridEvenToggle = JSON.parse(
    map_parameters.gridEvenToggle ? map_parameters.gridEvenToggle : false,
  );
  let gridHorizontalToggle = JSON.parse(
    map_parameters.gridHorizontalToggle
      ? map_parameters.gridHorizontalToggle
      : false,
  );

  // Set font size from height (all fonts are in 'em' and will be relative to this)
  let fontSize = hexHeight / 6;
  contentContainer.style.fontSize = fontSize + "px";

  // Calculate length of hexmap (max and rendered range)
  let xHexes = xEnd - (xStart - 1);
  let yHexes = yEnd - (yStart - 1);
  let xMaxLength;
  let yMaxLength;
  let xLength;
  let yLength;
  let xOffset;
  let yOffset;
  if (gridHorizontalToggle === false) {
    xMaxLength =
      (hexWidth - hexWidthOriginOffset) * map_parameters.xMax +
      hexWidthOriginOffset;
    yMaxLength = hexHeight * map_parameters.yMax + hexHeightOriginOffset;
    xLength = (hexWidth - hexWidthOriginOffset) * xHexes + hexWidthOriginOffset;
    yLength = hexHeight * yHexes + hexHeightOriginOffset;
    // Add offset if start of range is above 1
    xOffset =
      xStart === 1 ? 0 : -((hexWidth - hexWidthOriginOffset) * (xStart - 1));
    yOffset = yStart === 1 ? 0 : -(hexHeight * (yStart - 1));
  } else if (gridHorizontalToggle === true) {
    xMaxLength = hexHeight * map_parameters.xMax + hexHeightOriginOffset;
    yMaxLength =
      (hexWidth - hexWidthOriginOffset) * map_parameters.yMax +
      hexWidthOriginOffset;
    xLength = hexHeight * xHexes + hexHeightOriginOffset;
    yLength = (hexWidth - hexWidthOriginOffset) * yHexes + hexWidthOriginOffset;
    // Add offset if start of range is above 1
    xOffset = xStart === 1 ? 0 : -(hexHeight * (xStart - 1));
    yOffset =
      yStart === 1 ? 0 : -((hexWidth - hexWidthOriginOffset) * (yStart - 1));
  }

  /** Background **/
  let bgImageUrl = map_parameters.bgImageUrl;
  let bgImageNote = map_parameters.bgImageNote;
  let imagePath = bgImageNote
    ? "api/images/" + bgImageNote.noteId + "/" + bgImageNote.title
    : bgImageUrl;
  let map_background = document.createElement("div");
  map_background.className = "map-background";
  map_background.style.backgroundColor = imagePath ? "black" : "lightgray";
  contentContainer.appendChild(map_background);
  // Add background image if one is specified (from internal note is specified, or from external URL)
  if (imagePath) {
    // set image container with restricted size (based on generated range of hexes)
    let map_image_container = document.createElement("div");
    map_image_container.className = "map-image-container";
    map_image_container.style.width = `${xLength}px`;
    map_image_container.style.height = `${yLength}px`;

    // set image with full size (based on maximum size of hexmap)
    let map_image = document.createElement("div");
    map_image.className = "map-image";
    map_image.style.backgroundImage = "url(" + imagePath + ")";
    map_image.style.width = `${xMaxLength}px`;
    map_image.style.height = `${yMaxLength}px`;
    map_image.style.left = `${xOffset}px`;
    map_image.style.top = `${yOffset}px`;

    map_image_container.appendChild(map_image);
    contentContainer.appendChild(map_image_container);
  }

  /** Map Notes & Hexes **/
  // Get notes with specific labels
  const existing_notes = await api.runOnBackend(
    (xStart, xEnd, yStart, yEnd) => {
      // const hex_notes = api.getNotesWithLabel("hex_coords");
      const hex_notes = api.searchForNotes("#hex_coords");
      const existing_notes = [];
      for (let note of hex_notes) {
        let id = note.noteId;
        let title = note.title;
        let coordX = parseInt(
          note.getLabelValue("hex_coords").split(".")[0],
          10,
        );
        let coordY = parseInt(
          note.getLabelValue("hex_coords").split(".")[1],
          10,
        );
        let icon = note.getLabelValue("hex_icon");
        let cssClass = note.getLabelValue("cssClass");
        let targetRelations = note
          .getTargetRelations()
          .filter((e) => e.name === "current_location");
        let agentNotes = [];
        for (let agentNote of targetRelations) {
          let agent = agentNote.getNote();
          let agent_type = agent.getLabelValue("agent_type");
          let serializedAgent = agent.getPojo();
          serializedAgent.agent_type = agent_type;
          agentNotes.push(serializedAgent);
        }
        if (
          coordX &&
          coordX >= xStart &&
          coordX <= xEnd &&
          coordY &&
          coordY >= yStart &&
          coordY <= yEnd
        ) {
          existing_notes.push({
            id,
            title,
            coordX,
            coordY,
            icon,
            cssClass,
            agentNotes,
          });
        }
      }
      return existing_notes;
    },
    [xStart, xEnd, yStart, yEnd],
  );

  // Generate hex rows and items from start/end values
  for (let i = yStart; i <= yEnd; i++) {
    // Add and size hex row
    let hexRow = document.createElement("div");
    hexRow.className += " hex-row";

    // If in vertical mode, shift every row a bit, if in horizontal mode, shift odd or even rows right
    if (gridHorizontalToggle === false) {
      hexRow.style.marginLeft = hexWidthOriginOffset + "px";
    } else if (gridHorizontalToggle === true) {
      if (i === 1) {
        hexRow.style.marginTop = hexWidthOriginOffset + "px";
      }
      if (
        (gridEvenToggle === false && i % 2 === 0) ||
        (gridEvenToggle === true && i % 2 === 1)
      ) {
        hexRow.className += " shifted";
        hexRow.style.marginLeft = hexHeightOriginOffset + "px";
      }
    }

    for (let j = xStart; j <= xEnd; j++) {
      // Add hex items, size based on specified hex_height
      let hexItem = document.createElement("div");
      hexItem.className += " hex";
      hexItem.className +=
        gridHorizontalToggle === false ? " vertical" : " horizontal";

      // If in vertical mode, shift odd or even hexes down
      if (gridHorizontalToggle === false) {
        hexItem.style.width = hexWidth + "px";
        hexItem.style.height = hexHeight + "px";
        hexItem.style.marginLeft = -hexWidthOriginOffset + "px";
        hexItem.style.marginBottom = -hexHeightOriginOffset + "px";
        if (
          (gridEvenToggle === false && j % 2 === 0) ||
          (gridEvenToggle === true && j % 2 === 1)
        ) {
          hexItem.className += " shifted";
          hexItem.style.marginTop = hexHeightOriginOffset + "px";
        }
      } else if (gridHorizontalToggle === true) {
        hexItem.style.width = hexHeight + "px";
        hexItem.style.height = hexWidth + "px";
        //hexItem.style.marginLeft = hexHeightOriginOffset+'px';
        hexItem.style.marginTop = -hexWidthOriginOffset + "px";
      }

      // Prepare coords label
      let hexLabel = document.createElement("span");
      let hexLabelValue = "[" + j + "." + i + "]";
      hexLabel.className += " hex-label";

      // Check if note with existing coords already exist for custom label
      if (existing_notes.some((e) => e.coordX === j && e.coordY === i)) {
        // Get existing note and create a note link
        let note = existing_notes.find((e) => e.coordX === j && e.coordY === i);

        // Find any note that has a specific inverse relation to other notes
        if (note.agentNotes.length > 0) {
          let agentMarkers = document.createElement("div");
          agentMarkers.className += " hex-agents";
          hexItem.appendChild(agentMarkers);
          // add a marker for each agent note found, max 4 (above shows a "+" marker for overflow)
          for (let agentNote of note.agentNotes) {
            if (agentNote.agent_type === "players") {
              // If the agent note is of type "player", color hex background instead of adding agent dot
              hexItem.className += " players";
            } else {
              // prepare note link
              const agentNotePath = `root/${agentNote.noteId}`;
              const markerLink = document.createElement("a");
              markerLink.href = "#" + agentNotePath;
              markerLink.setAttribute("data-action", "note");
              markerLink.setAttribute("data-note-path", agentNotePath);
              markerLink.innerText = "";
              // make marker
              let agent = document.createElement("span");
              agent.className += " hex-agent";
              if (agentMarkers.childNodes.length < 3) {
                agent.appendChild(markerLink);
                let agentType = agentNote.agent_type
                  ? agentNote.agent_type
                  : "neutral";
                agent.className += " " + agentType;
                agentMarkers.appendChild(agent);
              } else if (agentMarkers.childNodes.length === 3) {
                agent.className += " overflow";
                agentMarkers.appendChild(agent);
              }
            }
          }
        }

        // If hex has an icon, add a big label with the note title
        const notePath = `root/${note.id}`;
        const hexLink = document.createElement("a");
        hexLink.href = "#" + notePath;
        hexLink.setAttribute("data-action", "note");
        hexLink.setAttribute("data-note-path", notePath);
        if (note.icon) {
          // Add custom class and element for icons
          let hexIcon = document.createElement("div");
          hexIcon.className += " hex-icon";
          hexItem.appendChild(hexIcon);
          hexItem.className += " " + note.icon;

          // change hexLink title
          hexLink.innerText = note.title;

          // Add title with the hexLink
          let hexTitle = document.createElement("span");
          hexTitle.className += " hex-title";
          hexTitle.appendChild(hexLink);
          hexTitle.className += " " + note.cssClass; //color based on note progress
          hexItem.appendChild(hexTitle);
        } else {
          // Without an icon and no title, add the hexLink to the label instead
          hexLink.innerText = hexLabelValue;
          hexLabel.appendChild(hexLink);
          hexLabel.className += " " + note.cssClass; //color based on note progress
          hexLabel.className += " link";
        }
      } else {
        // If no existing note, set default appearance to label
        // Add link to create new note named X.Y
        let hexNewLink = document.createElement("a");
        hexNewLink.href = "javascript:void(0)";
        hexNewLink.ondblclick = (function (hexLabelValue, j, i) {
          return function () {
            createNewHexNote(map_parameters, hexLabelValue, j, i);
            return false;
          };
        })(hexLabelValue, j, i);
        // Add label
        hexNewLink.appendChild(document.createTextNode(hexLabelValue));
        hexLabel.appendChild(hexNewLink);
        hexLabel.className += " nothing";
      }
      // Add label to item, add item to row
      hexItem.appendChild(hexLabel);
      hexRow.appendChild(hexItem);
    }
    // Add row to container
    contentContainer.appendChild(hexRow);
  }

  $(async function () {
    await centerView(viewContainer);
  });
}

// Function to create new note named after its coordinates
async function createNewHexNote(map_parameters, title, x, y) {
  console.log("Creating new Hex Note...");
  let newNoteRoot = map_parameters.newNoteRoot
    ? map_parameters.newNoteRoot
    : null;
  let newNoteTemplate = map_parameters.newNoteTemplate
    ? map_parameters.newNoteTemplate
    : null;
  const createdNote = await api.runOnBackend(
    (title, x, y, newNoteRoot, newNoteTemplate) => {
      const { note } = api.createTextNote(
        newNoteRoot ? newNoteRoot.noteId : "root",
        title,
        "",
      );
      if (newNoteTemplate) {
        note.setRelation("template", newNoteTemplate.noteId);
      }
      let coords =
        parseInt(x, 10).toString() + "." + parseInt(y, 10).toString();
      note.setLabel("hex_coords", coords);
      return note;
    },
    [title, x, y, newNoteRoot, newNoteTemplate],
  );
  console.log(
    "New Hex Note '" +
      createdNote.title +
      "' created under '" +
      (newNoteRoot ? newNoteRoot.title : "root") +
      "', with template '" +
      (newNoteTemplate ? newNoteTemplate.title : "NONE") +
      "'.",
  );
  await api.openTabWithNote(createdNote.noteId, true);
}

async function centerView(viewContainer) {
  viewContainer.style.scrollBehavior = "auto";
  // put starting scroll position in the center
  let width = viewContainer.scrollWidth;
  let win_width = viewContainer.clientWidth;
  let height = viewContainer.scrollHeight;
  let win_height = viewContainer.clientHeight;
  viewContainer.scrollTop = height / 2 - win_height / 2;
  viewContainer.scrollLeft = width / 2 - win_width / 2;
}

async function makeDragScrollable(viewContainer) {
  viewContainer.style.cursor = "grab";
  let pos = { top: 0, left: 0, x: 0, y: 0 };
  const mouseDownHandler = function (e) {
    viewContainer.style.cursor = "grabbing";
    viewContainer.style.userSelect = "none";
    pos = {
      left: viewContainer.scrollLeft,
      top: viewContainer.scrollTop,
      x: e.clientX,
      y: e.clientY,
    };
    document.addEventListener("mousemove", mouseMoveHandler);
    document.addEventListener("mouseup", mouseUpHandler);
  };
  const mouseMoveHandler = function (e) {
    const dx = e.clientX - pos.x;
    const dy = e.clientY - pos.y;
    viewContainer.scrollTop = pos.top - dy;
    viewContainer.scrollLeft = pos.left - dx;
  };
  const mouseUpHandler = function () {
    viewContainer.style.cursor = "grab";
    viewContainer.style.removeProperty("user-select");
    document.removeEventListener("mousemove", mouseMoveHandler);
    document.removeEventListener("mouseup", mouseUpHandler);
  };
  viewContainer.addEventListener("mousedown", mouseDownHandler);
}
